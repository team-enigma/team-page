---
title: Welcome to our website!
about: We are a group of young, bright minds with varied interests within the sphere of software development. As a team with a considerable amount of industry experience, we possess the knowledge and expertise to effectively deliver high-quality software solutions that meet our standard for excellence. We are all passionate, dedicated and professional individuals who look forward to taking on new challenges for our project owners.
project: The idea of leveraging peer-to-peer software to directly connect clients and service providers has been explored recently in the taxi, restaurant and accommodation industries with apps like Uber, Lyft, Uber Eats, and Airbnb. Our project owners want to apply this idea to a major industry in our country ​— ​transport logistics. This fills an actual need for both truck fleet managers and companies that require bulk deliveries. To find out more, head on over to our Documentation Hub.
img: 
---