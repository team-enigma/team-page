---
title: "Mobile Coding Standards"
date: 2020-09-03T08:15:31+02:00
languages:
  - icon-flutter
  - devicons devicons-dart
link: /other/docs/Coding_Standards_Mobile.pdf
---
