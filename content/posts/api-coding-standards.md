---
title: "API Coding Standards"
date: 2020-07-21T11:28:31+02:00
languages:
  - icon-spring
  - icon-kotlin
link: /other/docs/Coding Standards.pdf
---