---
firstName: Malick
lastName: Burger
email: malickjackburger@gmail.com
# gradient for homepage
# ---
color1: "#f8ff00"
color2: "#3ad591"
# ---
profileBackground: bg10.jpg
profileImg: malick.png # image name - place image under static/img/malick
job: 3rd year BIT
cv: CV.pdf # place under static/other/{your_name}/{file_name} or place a link to your cv
tags:
  - name: Web Developer
  - name: Mobile Developer
  - name: API Developer
  - name: System Administrator
  - name: Database Administrator
  - name: Docker Enthusiast
  - name: Linux Advocate
  - name: Stack Overflow Fanboy
social:
  - name: GitHub
    link: https://github.com/MalickBurger
    icon: fa fa-github
  - name: GitLab
    link: https://gitlab.com/MalickBurger
    icon: fa fa-gitlab
  - name: LinkedIn
    link: https://linkedin.com/in/MalickBurger
    icon: fa fa-linkedin
experience: 
  - company: EyeSite
    position: Founder & Lead Developer
    date: 2018 - Present
    description: >
      Large scale solar system and generator monitoring solution, designed and implemented in collaboration with Energenic. EyeSite aggregates data from various devices such as PV inverters, 
      battery monitors, solar chargers, and genset controllers. EyeSite provides custom alarms on any aggregated data and is mainly used for monitoring large data and as an early warning system. 
      This system is developed using a microservices approach utilizing Docker containers making it highly scalable.
      The system makes use of Docker Swarm to allow for loadbalancing and high availability of services, mainly utilized by the backend API which sees intense data throughput.
  - company: University of Pretoria
    position: System Administrator
    date: 2019 - Present
    description: >
      System development and administration for services in use by the department of computer science.
  - company: University of Pretoria
    position: Compute Cluster Administrator
    date: 2018 - 2019
    description: Compute Cluster administration and development.
  - company: Icon Electronics
    position: Part time Technician
    date: 2015-2018
    description: PCB assembly and soldering.
skills:
  # all icons:
  # https://konpa.github.io/devicon/
  # https://vorillaz.github.io/devicons/#/cheat
  # /static/css/custom-icons
  - name: C
    icon: devicon-c-plain
  - name: Android
    icon: devicon-android-plain
  - name: Bootstrap
    icon: devicon-bootstrap-plain
  - name: Express
    icon: devicon-express-original
  - name: Java
    icon: devicon-java-plain
  - name: Javascript
    icon: devicon-javascript-plain
  - name: Jquery
    icon: devicon-jquery-plain
  - name: MySQL
    icon: devicon-mysql-plain
  - name: Nginx
    icon: devicon-nginx-plain
  - name: PHP
    icon: devicon-php-plain
  - name: PostgreSQL
    icon: devicon-postgresql-plain
  - name: Typescript
    icon: devicon-typescript-plain
  - name: Node.js
    icon: devicon-nodejs-plain
  - name: Django
    icon: devicon-django-plain
  - name: Go
    icon: icon-go
  - name: Flutter
    icon: icon-flutter
  - name: C++
    icon: devicon-cplusplus-plain
  - name: Apache
    icon: devicon-apache-plain
  - name: HTML
    icon: devicon-html5-plain
  - name: CSS
    icon: devicon-css3-plain
  - name: Python
    icon: devicon-python-plain
devops:
  - name: Mocha
    icon: devicon-mocha-plain
  - name: Firebase
    icon: devicons devicons-firebase
  - name: AWS
    icon: devicon-amazonwebservices-original
  - name: LXC/LXD
    icon: fa fa-box
  - name: Docker
    icon: devicon-docker-plain
  - name: Docker Swarm
    icon: fa fa-server
  - name: GitHub
    icon: devicon-github-plain
  - name: GitLab
    icon: devicon-gitlab-plain
  - name: BitBucket
    icon: devicon-bitbucket-plain
  - name: Git
    icon: devicon-git-plain 
other:
  - name: Problem Solving
    icon: fa fa-brain
  - name: Project Management
    icon: devicon-trello-plain
  - name: System Design
    icon: fa fa-cogs
  - name: Security Conscious
    icon: fa fa-fingerprint
education:
  - title: BIT Top Achiever
    institution: University of Pretoria
    date: 2020
    description: >
      Awarded for being the top GPA achieving second year BIT student at the University of Pretoria for 2019.
    link: BIT_Top_Achiever.pdf
  - title: Bachelor of Information Technology (BIT)
    institution: University of Pretoria
    date: 2018
    description: >
      A BIT is a notably challenging four-year programme which integrates the different disciplines related to information technology.
      The fourth and final year includes a six months' learnership with participating organisations, where students are employed as trainees.
    link: Academic_Record.pdf
  - title: Golden Key Intl. Honor Society
    institution: University of Pretoria
    date: 2019
    description: >
      Golden Key has chapters at colleges and universities in Australia, The Bahamas, Canada, India, Malaysia, New Zealand, Singapore,
      South Africa, and the United States. Membership into Golden Key is offered to undergraduate and graduate students recognized to be among the
      top 15% of their class by GPA. Lifetime membership is given to those who pay a one-time fee.
    link: Golden_Key.pdf
  - title: National Senior Certificate
    institution: Jeppe High School for Boys
    date: 2017
    description: >
      I achieved three distinctions, notably in Physical Sciences and Information Technology when I matriculated.
  - title: U19 Provincial Rowing Colours
    institution: South African Schools Rowing Union
    date: 2016
    description: >
      Received U19 provincial colours and set two South African rowing course records in both the coxless pair and the coxed four.
  - title: U16 Provincial Rowing Colours
    institution: South African Schools Rowing Union
    date: 2015
    description: >
      Received U16 provincial colours and travelled to England as a member of the South African Development (junior) rowing team
      to partake in a number of regattas.
projects:
  - title: Mouthpiece App
    role: Integration Team
    date: February 2020
    description: >
      Team leader for an integration team overseeing over 40 colleagues tasked at creating a mobile application that synchronizes 
      the lip movements of an animated image of a mouth with a voice stream recorded using the phone’s microphone.
      I acted as System designer, API developer, and system administrator. The back-end system was implemented by our
      team using a microservice approach utilizing docker and docker-compose.
    image: mouthpiece.jpg
  - title: EyeSite
    role: Founder & Lead Developer
    date: 2018
    description: >
      Large scale solar system and generator monitoring solution, designed and implemented in collaboration with Energenic. EyeSite aggregates data from various devices such as PV inverters, 
      battery monitors, solar chargers, and genset controllers. EyeSite provides custom alarms on any aggregated data and is mainly used for monitoring large data and as an early warning system. 
      This system is developed using a microservices approach utilizing Docker containers making it highly scalable.
      The system makes use of Docker Swarm to allow for loadbalancing and high availability of services, mainly utilized by the backend API which sees intense data throughput.
    image: Eyesite.png
about: >
  I am a third year Information Technology student at the University of Pretoria majoring in Computer Science, I have been working at the
  university since my first year in 2018. I initially worked as a data cluster administrator for a cluster used by big data master students.
  In my second year (2019) I changed jobs to being a system administrator for the computer science department where I manage a number of servers and web interfaces.
  I am an inspired, highly driven individual always looking to grasp a deeper understanding in all that I do to ensure that I can perform any task at the highest standard.
---
