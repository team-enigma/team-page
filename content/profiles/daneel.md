---
firstName: Daneel
lastName: Nortier
email: u18019634@tuks.co.za
# gradient for hompage
# ---
color1: "#FDBB2D"
color2: "#3A1C71"
# ---
profileBackground: bg2.jpg
profileImg: daneel.jpeg # image name - place image under static/img/daneel
job: 3rd Year Mathematics
cv: cv.pdf
tags:
  - name: Back-end developer
  - name: Mathematician
  - name: Artificial Intelligence
social:
  - name: GitHub
    link: https:github.com/daneelnortier
    icon: fa fa-github
  - name: GitLab
    link: https:gitlab.com/daneelnortier
    icon: fa fa-gitlab
    
skills:
  # all icons: 
  # https://konpa.github.io/devicon/
  # https://vorillaz.github.io/devicons/#/cheat
  # /static/css/custom-icons
  - name: C++
    icon: devicon-cplusplus-plain
  - name: Python
    icon: devicon-python-plain
  - name: Java
    icon: devicon-java-plain
  - name: MySQL
    icon: devicon-mysql-plain
devops:
  - name: GitHub
    icon: devicon-github-plain
  - name: GitLab
    icon: devicon-gitlab-plain
  - name: Git
    icon: devicon-git-plain 
other:
  - name: Problem-solving
    icon: fa fa-brain
  - name: Mathematics
    icon: fas fa-square-root-alt   
education:
  - title: National Senior Certificate
    institution: Pretoria Boys High School 
    date: 2012-2017
    description: I completed my high school diploma with 5 out of 7 distinctions, and an average of 80.7%. My chosen subjects where Engineering and Graphics Design, Science and History.
    link: NSC.pdf
  - title: Golden Key Intl. Honor Society
    institution: University of Pretoria
    date: 2018-2020
    description: I have been invited to join the golden key society.
  - title: BSc Mathematics
    institution: University of Pretoria
    date: 2018-2020
    description: I am currently in my final year of this degree, which covers a wide range of mathematical topics including statistics, calculus and algebra. I also take a number of computer science modules.
    link: UP_ACAD_RCD.pdf
  - title: Catalyst Coding Contest
    institution: University of Pretoria
    date: 2019
    description: I competed in the catalyst coding contest in a team of 3 people, and achieved a result of second place.
    link: CCC.pdf    
projects:
  - title: Mouthpiece App
    role: Integration Team
    date: February 2020
    description: I managed one of the sub-groups for this project.
      Supporting role in API development.
    image: prj1.jpg
about: I am a third year mathematics and computer science student, whom is always ready to learn new skills and take on new challenges. My mathematical background, used in conjunction with my computer science skills are always useful for solving a variety of problems on both the mathematical and computer science sides of my degree.
---