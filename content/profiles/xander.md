---
firstName: Xander
lastName: Terblanche
email: u18070788@tuks.co.za
# for homepage gradient
# ----
color1: "#00d2ff"
color2: "#3a47d5"
# ----
profileBackground: bg9.jpg
profileImg: xander.jpg # image name - place image under static/img/xander
job: 3rd year computer science
cv: # place under static/other/{your_name}/{file_name} or place a link to your cv
tags:
  - name: mobile developer
  - name: registered android developer
  - name: registered apple developer
  - name: full stack
social:
  - name: GitHub
    link: https://github.com/xanderterbl1
    icon: fa fa-github
experience: 
  - company: University of Pretoria
    position: System Administrator
    date: 2019 - Present
    description: System development and administration for services in use by the department of computer science.
  - company: Cognian Systems
    position: Full Stack Mobile Developer
    date: 2018 - 2020
    description: Head mobile developer in creating a "Smart School"-application for a startup called Cognian Systems.
  - company: Past Papers ZA
    position: Founder & Developer
    date: 2017 - 2020
    description: Developed a Past Paper Application (Android & iOS & Web), currently crossing +200 000 downloads.
skills:
  # all icons: 
  # https://konpa.github.io/devicon/
  # https://vorillaz.github.io/devicons/#/cheat
  # /static/css/custom-icons
  - name: Javascript
    icon: devicon-javascript-plain
  - name: C++
    icon: devicon-cplusplus-plain
  - name: Django
    icon: devicon-django-plain
  - name: Node.js
    icon: devicon-nodejs-plain
  - name: Xamarin
    icon: icon-xamarin
  - name: Objective-C
    icon: icon-c
  - name: Flutter
    icon: icon-flutter
  - name: C#
    icon: devicon-csharp-plain
  - name: Dart
    icon: devicons devicons-dart
devops:
  - name: Docker
    icon: devicon-docker-plain
  - name: GitHub
    icon: devicon-github-plain
  - name: GitLab
    icon: devicon-gitlab-plain
  - name: Git
    icon: devicon-git-plain 
other:
  - name: Problem-solving
    icon: fa fa-brain
education:
  - title: National Senior Certificate
    institution: Hoerskool Kempton Park
    date: 2017
    description: Obtained this diploma with eight distinctions and an aggregate of 92%. My elective subjects included Accounting, Physical Sciences, IT. (This includes AP Mathematics (IEB) where I obtained a final average of 90% )
  - title: 3rd Year - BSc Computer Science
    institution: University of Pretoria
    date: 2017
    description: Currently 3rd year Computer science @ UP with a weighted average of 77%
  - title: Golden Key Intl. Honor Society
    institution: University of Pretoria
    date: 2018 - 2019
    description: Golden Key has chapters at colleges and universities in Australia, The Bahamas, Canada, India, Malaysia, New Zealand, Singapore, South Africa, and the United States. Membership into Golden Key is offered to undergraduate and graduate students recognized to be among the top 15% of their class by GPA. Lifetime membership is given to those who pay a one-time fee.
  - title: Academic Excellence Award
    institution: Ekurhuleni
    date: 2017 
    description: Received an award from the Executive Mayor of Ekurhuleni for achieving 9th place in the Ekurhuleni Public Schools Top 15 (92%). (3rd place in Ekurhuleni North District)
projects:
  - title: TuksFM Mobile App
    role: Co-developer
    date: March 2019
    description: Acted primarily as back-end developer (MySQL + PHP API for legacy compatibility; Dart & Flutter for app-side API calls).
    image: tuksfm.jpg
  - title: Past Papers 
    role: Founder & Developer
    date: 2017 - 2020
    description: Developed a Past Paper Application (Android & iOS & Web), currently crossing +200 000 downloads.
    image: prj2.jpg
    link: https://past-papers.co.za

about: I am an experienced systems admin and software developer (web, mobile and desktop). I have made and launched websites and have multiple app titles published on android and iOS app stores. I am well motivated, passionate and always up for a challenge.
---