---
firstName: Francois
lastName: Snyman
email: francois.snyman@tuks.co.za
# gradient for homepage
# ---
color1: "#203053"
color2: "#72BBC9"
# color3: "#f5ef98" # Not a thing :'(

# ---
profileBackground: bg12.jpg
profileImg: francois.jpg
job: 3rd year computer science
cv: cv-light.pdf
tags:
  - name: Mobile Development
  - name: Dart & Flutter
  - name: UI/UX Design
  - name: Web Development
  - name: HTML/CSS/JS
  - name: API Development
  - name: LAMP / LAPP / LEMP
  - name: Systems Development

social:
  - name: GitHub
    link: https://github.com/fjsnyman
    icon: fa fa-github
  - name: Website
    link: https://fjsnyman.co.za
    icon: fa fa-globe-africa
  - name: LinkedIn
    link: https://www.linkedin.com/in/fjsnyman
    icon: fa fa-linkedin

experience: 
  - company: FirstView Media
    position: Intern
    date: March, December 2017
    description: Acted primarily as a back-end developer, using MySQL, PHP, and JS across multiple websites and API development projects. These projects included significant server-management tasks, client interaction, and responsibility for the full stack (HTML/CSS/JS/PHP) on smaller projects.

  - company: Kwaden Software Development
    position: Front-End Developer
    date: March & December 2019
    description: Played several roles to gain a broad experience with Spring Boot (Java), process-driven team collaboration, and microservice architectures.

skills:
  # all icons: 
  # https://konpa.github.io/devicon/
  # https://vorillaz.github.io/devicons/#/cheat
  # /static/css/custom-icons
  - name: C++
    icon: devicon-cplusplus-plain
  - name: CSS
    icon: devicon-css3-plain
  - name: Dart
    icon: devicons devicons-dart
  - name: Flutter
    icon: icon-flutter
  - name: Go
    icon: icon-go
  - name: HTML
    icon: devicon-html5-plain
  - name: Java
    icon: devicon-java-plain
  - name: Javascript
    icon: devicon-javascript-plain
  - name: MySQL
    icon: devicon-mysql-plain
  - name: Node.js
    icon: devicon-nodejs-plain
  - name: PHP
    icon: devicon-php-plain
  - name: Spring
    icon: icon-spring

devops:
  - name: Git
    icon: devicon-git-plain 
  - name: BitBucket
    icon: devicon-bitbucket-plain
  - name: GitHub
    icon: devicon-github-plain
  - name: GitLab
    icon: devicon-gitlab-plain
  - name: Jira
    icon: devicons devicons-jira
  - name: Docker
    icon: devicon-docker-plain
  - name: Kubernetes
    icon: icon-kubernetes

other:
  - name: Team Management
    icon: fa fa-users
  - name: Professionalism
    icon: fa fa-user-tie
  - name: Clear Communication
    icon: fa fa-people-arrows
  - name: Creative Thinking
    icon: fa fa-user-astronaut
  - name: Client Interaction
    icon: fa fa-baby

education:
  - title: Ongoing BSc Computer Science 
    institution: University of Pretoria
    date: 2018 - now
    description: Have and continue to maintain a high standard of work throughout this notoriously difficult course, achieving several distinctions and receiving multiple invitations to join the International Golden Key Society.
    link: up_academic_record_2019.pdf

  - title: National Senior Certificate
    institution: Independent Examination Board (IEB)
    date: 2016
    description: Scored outstanding or meritorious achievement level for all subjects, including several subject prizes and olympiad awards for Maths and IT.
    link: nsc_2016.pdf

  - title: Student Member of the IITPSA
    institution: The Institute of Information Technology Professionals South Africa
    date: 2016
    description: An extension of my participation in the 2016 IT Olympiad was being granted membership to the IITPSA as a mark of excellence.
    link: iitpsa_membership_2016.pdf

  - title: IT Olympiad Finalist
    institution: South African Computing Olympiad (SACO)
    date: 2016
    description: Tied first in KZN provincial qualifiers, and came in the final top 10 at the national competition
    link: it_olympiad_2016.pdf

projects:
  - title: Mouthpiece App
    role: Integration Team
    date: February 2020
    description: Lead collaboration with over 40 colleagues within sub-teams, maintained Git repositories and business rules, and managed my Integration team's project management tools. Acted as lead developer for mobile app back-end, controllers, and unit testing.
    image: mouthpiece.jpg

  - title: TuksFM Mobile App
    role: Co-developer
    date: March 2019
    description: Acted primarily as back-end developer (MySQL + PHP API for legacy compatibility; Dart & Flutter for app-side API calls).
    image: tuksfm.jpg

  - title: Active Projects
    role: Developer
    date: 2020
    description: Currently working on a few ongoing projects to continuously bolster my skills and experience.
    image: prj3.jpg

about: Several years of working experience has given me strong software development skills with mobile apps, RESTful APIs, and high-efficiency interaction between the two. I am highly skilled in mobile development for iOS and Android using Dart & Flutter, and web development using Apache/MySQL/PHP/JS/HTML/CSS or frameworks such as NodeJS and SpringBoot.
---