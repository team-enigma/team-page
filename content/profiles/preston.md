---
firstName: Preston
lastName: van Tonder
email: p.vantonder@tuks.co.za
color1: "#d53369"
color2: "#daae51"
profileBackground: bg13.jpg
tint: primary
profileImg: profile.png
job: 3rd year applied mathematics
cv: cv.pdf
tags:
  - name: back-end developer
  - name: artificial intelligence
  - name: API developer
social:
  - name: GitHub
    link: https://github.com/prestonvtonder
    icon: fa fa-github
  - name: GitLab
    link: https://gitlab.com/prestonvtonder
    icon: fa fa-gitlab
  - name: LinkedIn
    link: https://www.linkedin.com/in/prestonvtonder
    icon: fa fa-linkedin
experience: 
  - company: University of Pretoria - Artificial Intelligence
    position: Research Assistant
    date: Present
    description: Gaining experience in Convolutional Neural Networks and Transfer Learning techniques used to detect hip dysplasia in canines. Furthermore, due to the current situation, I have also been tasked with trying to develop a neural network capable of detecting COVID-19 from chest X-ray images and CT scans.
  - company: University of Pretoria - Applied Mathematics
    position: Intern
    date: December 2017 - November 2018
    description: I was primarily responsible for editing Conference Contributions for the BIOMATH Conference that took place in 2018. This resulted in an in-depth understanding of LaTex and also involved communication between myself and Professors from across the world. I was working under the HOD of the Department of Mathematics and Applied Mathematics, Prof. Roumen Anguelov. 
skills:
  - name: C++
    icon: devicon-cplusplus-plain
  - name: Java
    icon: devicon-java-plain
  - name: HTML
    icon: devicon-html5-plain
  - name: CSS
    icon: devicon-css3-plain
  - name: Javascript
    icon: devicon-javascript-plain
  - name: Node.js
    icon: devicon-nodejs-plain
  - name: Express
    icon: devicon-express-original
  - name: jQuery
    icon: devicon-jquery-plain
  - name: PHP
    icon: devicon-php-plain
  - name: TypeScript
    icon: devicon-typescript-plain
  - name: Python
    icon: devicon-python-plain
  - name: Bootstrap
    icon: devicon-bootstrap-plain
  - name: MySQL
    icon: devicon-mysql-plain
  - name: Hugo
    icon: icon-hugo
  - name: TensorFlow
    icon: icon-tensorflow
  - name: Sass
    icon: devicon-sass-original
devops:
  - name: Docker
    icon: devicon-docker-plain
  - name: GitHub
    icon: devicon-github-plain
  - name: GitLab
    icon: devicon-gitlab-plain
  - name: Git
    icon: devicon-git-plain 
  - name: Mocha
    icon: devicon-mocha-plain
  - name: npm
    icon: devicons devicons-npm
other:
  - name: Problem solving
    icon: fa fa-brain
  - name: Critical thinking
    icon: fa fa-cogs
  - name: Responsible
    icon: fa fa-hourglass-half
  - name: Team management
    icon: fa fa-users
  - name: Professionalism
    icon: fa fa-user-tie
  - name: Mathematics
    icon: fa fa-square-root-alt
education:
  - title: Bachelor of Science in Applied Mathematics
    institution: University of Pretoria
    date: Present
    description: Application of mathematical methods by different fields such as science, engineering, business, computer science, and industry. Furthemore, I have chosen to do a second major in computer science, focusing primarily on web development and software engineering principles. 
    link: UP Academic Record.pdf
  - title: CSI Bursary Top Achiever Award
    institution: Investec
    date: July 2019
    description: I received this award for placing first amongst all first-year Investec CSI Bursary students at Investec's annual Mentorship Day. 
    link: Investec Certificate.pdf
  - title: Golden Key Intl. Honour Society
    institution: University of Pretoria
    date: May 2019
    description: Golden Key has chapters at colleges and universities in Australia, The Bahamas, Canada, India, Malaysia, New Zealand, Singapore, South Africa, and the United States. Membership into Golden Key is offered to undergraduate and graduate students recognized to be among the top 15% of their class by GPA. Lifetime membership is given to those who pay a one-time fee.
  - title: Dean's Merit List of Exceptional Academic Achievers
    institution: University of Pretoria
    date: May 2019
    description: This award was received during the Annual Exceptional Achievers' Function for Students. I placed sixth in the Faculty of Natural and Agricultural Sciences with an aggregate of 91.23%.
    link: Dean Merit List Award.pdf
  - title: Certificate in Advanced English
    institution: Cambridge English Language Assessment
    date: February 2018
    description: C1 Advanced is a thorough test of all areas of language ability. The exam is made up of four papers developed to test your English language skills. The Speaking test is taken face to face, with two candidates and two examiners. This creates a more realistic and reliable measure of your ability to use English to communicate.
    link: Cambridge English Certificate.pdf
  - title: National Senior Certificate
    institution: Jeppe High School for Boys
    date: November 2017
    description: Obtained this diploma with seven distinctions and an aggregate of 92%. My elective subjects included Accounting, Physical Sciences, Life Sciences as well as Advanced Programme Mathematics.
    link: NSC - Preston van Tonder.pdf
  - title: Advanced Programme Mathematics
    institution: Independent Examinations Board
    date: November 2017
    description: As an extra subject, this involved preparation in the form of exposure to concepts and branches that are part and parcel of the first year university studies in Mathematics, Physics and related courses; the emphasis is on subject content and giving learners a fundamental understanding of advanced mathematical and scientific concepts. I ultimately obtained an aggregate of 85% with an elective in Statistics.
    link: IEB - Preston van Tonder.pdf
  - title: Dux Scholar Award
    institution: Jeppe High School for Boys
    date: October 2017
    description: I received this award during our Valedictory Ceremony as a result of placing first in my grade during my matric year.
    link: Dux Scholar Award.pdf
projects:
  - title: Mouthpiece App
    role: Integration Team
    date: February 2020
    description: A fun mobile application that synchronizes the lip movements of an animated image of a mouth with a voice stream recorded using the phone’s microphone. As part of the Integration Team I am involved with managing sub-teams and developing the required API for the web and mobile applications.
    image: prj1.jpg
  - title: Hugo Static Website
    role: Web Developer
    date: April 2020
    description: As a fun little project, I decided that I would design a website for our team, Enigma, as a way to truly showcase our combined abilites to our potential project owners. In order to create this website I used the Hugo framework combined with the Now UI theme.
    image: hugo.jpg
    link: https://enigmasoftware.co.za
about: As a motivated final-year BSc Applied Mathematics student I have been exposed to several methods of approaching a problem. Although my degree is in Applied Mathematics, I am also doing a second major in Computer Science. This has helped me gain extensive knowledge in various programming languages and frameworks. Being careful not to forget my roots, I marry these two fields whenever I am faced with a problem and as a result, I believe I would be a valuable member of any team.
---
