// Set a variable for our button element.
const scrollToTopButton = document.getElementById('js-top');

// Let's set up a function that shows our scroll-to-top button if we scroll beyond the height of the initial window.
const scrollFunc = () => {
  // Get the current scroll value
  let y = window.scrollY;

  // If the scroll value is greater than the window height, let's add a class to the scroll-to-top button to show it!
  if (y > document.documentElement.clientHeight - 65) {
    scrollToTopButton.className = "btn-scroll-top show";
  } else {
    scrollToTopButton.className = "btn-scroll-top hide";
  }
};

window.addEventListener("scroll", scrollFunc);

const scrollToTop = () => {
  // Let's set a variable for the number of pixels we are from the top of the document.
  const c = document.documentElement.scrollTop || document.body.scrollTop;

  // If that number is greater than 0, we'll scroll back to 0, or the top of the document.
  // We'll also animate that scroll with requestAnimationFrame:
  // https://developer.mozilla.org/en-US/docs/Web/API/window/requestAnimationFrame
  if (c > 0) {
    window.requestAnimationFrame(scrollToTop);
    // ScrollTo takes an x and a y coordinate.
    // Increase the '10' value to get a smoother/slower scroll!
    let options = { behavior: 'smooth' }
    window.scrollTo(options)
    window.scrollTo(0, c - c / 10);
  }
};

// When the button is clicked, run our ScrolltoTop function above!
scrollToTopButton.onclick = function (e) {
  e.preventDefault();
  scrollToTop();
}

// Theme toggler
localStorage.getItem("theme") === "dark" ? $("body").attr("data-theme", "dark") : null;
$("#btn-theme").click(function () {
  if ((localStorage.getItem("theme") || "light") === "light") {
    localStorage.setItem("theme", "dark");
    $("body").attr("data-theme", "dark");
  } else if (localStorage.getItem("theme") === "dark") {
    localStorage.setItem("theme", "light");
    $("body").attr("data-theme", "light");
  }
  // $("body").attr("data-theme") === "light" ? $("body").attr("data-theme", "dark") : $(body).attr("data-theme", "light");
  // $("[data-theme]").each(function() {
  //   $(this).attr("data-theme") === "light" ? $(this).attr("data-theme", "dark") : $(this).attr("data-theme", "light");
  // });
});