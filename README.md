# Team Page

## Step 1: Install Hugo

You can install Hugo by following these [instructions](https://gohugo.io/getting-started/installing/)

## Step 2: Clone Recursively

```bash
git clone --recurse-submodules <pathspec>
```

## Step 3: Run Hugo Server

Launch Hugo server with:

```bash
hugo server -D
```

Then navigate to `localhost:1313`

## Step 4: Modify Content

Navigate to the `content/profiles` directory. Open the markdown file with your name and add the desired content.
